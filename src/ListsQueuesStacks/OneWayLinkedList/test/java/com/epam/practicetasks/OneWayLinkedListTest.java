package com.epam.practicetasks;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class OneWayLinkedListTest {

    @Test
    public void testEmptyListLength() {
        OneWayLinkedList<Object> list = new OneWayLinkedList<>();
        assertTrue("Empty list should have zero length", list.length() == 0);
    }

    @Test
    public void testInsertIntoEmptyList() {
        OneWayLinkedList<Object> list = new OneWayLinkedList<>();
        list.insert(new Object());
        assertTrue("When inserting one item into empty list its length should be 1", list.length() == 1);

    }

    @Test
    public void testNotEmptyListLength() {
        OneWayLinkedList<Object> list = createOneWayLinkedList(10);
        assertTrue("Length of list with 10 objects should be 10", list.length() == 10);
    }

    @Test
    public void testInsertIntoNotEmptyList() {
        OneWayLinkedList<Object> list = createOneWayLinkedList(10);
        int lengthBeforeInsert = list.length();
        list.insert(new Object());
        int lengthAfterInsert = list.length();
        assertTrue("When inserting one item into NOT empty list its length should increase by 1",
                (lengthAfterInsert - lengthBeforeInsert) == 1);

    }

    private static OneWayLinkedList<Object> createOneWayLinkedList(int length) {
        OneWayLinkedList<Object> list = new OneWayLinkedList<>();
        for (int i = 0; i < length; i++) {
            list.insert(new Object());
        }
        return list;
    }
}
