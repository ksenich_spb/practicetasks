package com.epam.practicetasks;

public class OneWayLinkedListUser {
    public static void main(String[] args) {

        OneWayLinkedList<Object> list0 = new OneWayLinkedList<>();
        list0.oneWayLinkedListInfo();

        OneWayLinkedList<Object> list1 = new OneWayLinkedList<>();
        list1.insert(new Object());
        list1.oneWayLinkedListInfo();

        OneWayLinkedList<Integer> list2 = new OneWayLinkedList<>();
        list2.insert(1);
        list2.insert(2);
        list2.oneWayLinkedListInfo();

        OneWayLinkedList<String> list3 = new OneWayLinkedList<>();
        list3.insert("a");
        list3.insert("b");
        list3.insert("c");
        list3.oneWayLinkedListInfo();
    }
}
