package com.epam.practicetasks;

public class OneWayLinkedList<E> {

    private int length = 0;

    private Node<E> first;

    public OneWayLinkedList() {
    }

    public void insert(E e) {
        Node<E> node = new Node(e, first);
        first = node;
        length++;
    }

    public int length() {
        return length;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("[");
        Node node = first;
        while (node != null) {
            s.append(node.item.toString());
            node = node.next;
            if (node != null) s.append(", ");
        }
        s.append("]");
        return s.toString();
    }

    public void oneWayLinkedListInfo() {
        System.out.println("list size = " + length());
        System.out.println("list = " + toString());
    }

    private static class Node<E> {
        E item;
        Node<E> next;

        Node(E element, Node<E> next) {
            this.item = element;
            this.next = next;
        }
    }


}
