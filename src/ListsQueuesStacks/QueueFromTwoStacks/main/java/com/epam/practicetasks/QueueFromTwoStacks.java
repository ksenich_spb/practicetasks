package com.epam.practicetasks;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class QueueFromTwoStacks<E> {

    private LinkedList<E> stackForInserting = new LinkedList<>();
    private LinkedList<E> stackforRemoving = new LinkedList<>();

    private int size = 0;

    public QueueFromTwoStacks(){
    }

    public void insert(E e){
        stackForInserting.push(e);
        size++;
    }

    public E remove(){
        if (size == 0) throw new NoSuchElementException();
        size--;
        if (!stackforRemoving.isEmpty()) {
            return stackforRemoving.pop();
        } else {
            while(stackForInserting.peek() != null){
                E item = stackForInserting.pop();
                stackforRemoving.push(item);
            }
            return stackforRemoving.pop();
        }
    }

    public int size(){
      return size;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("[");
        int initialSize = size;
        for (int i = 0; i < initialSize; i++) {
            E item = remove();
            s.append(item + ";");
            insert(item);
        }
        s.append("]");
        return s.toString();
    }
}
