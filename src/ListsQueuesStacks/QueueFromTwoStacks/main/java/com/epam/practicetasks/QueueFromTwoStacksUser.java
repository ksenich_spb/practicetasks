package com.epam.practicetasks;

public class QueueFromTwoStacksUser {

    public static void main(String[] args) {
        QueueFromTwoStacks<Integer> queue = new QueueFromTwoStacks<>();

        for (int i = 0; i < 10; i++) {
            queue.insert(i);
        }
        System.out.println("size = " + queue.size());
        System.out.println("queue = " + queue);

        for (int i = 0; i < 5; i++) {
            queue.remove();
        }
        System.out.println("size = " + queue.size());
        System.out.println("queue after removing 5 elements = " + queue);
    }
}
