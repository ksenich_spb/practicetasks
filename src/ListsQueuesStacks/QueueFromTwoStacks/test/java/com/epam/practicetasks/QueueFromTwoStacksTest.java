package com.epam.practicetasks;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertTrue;

public class QueueFromTwoStacksTest {

    @Test
    public void testEmptyQueueSize() {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        assertTrue("Size of empty queue should be zero", queue.size() == 0);
    }

    @Test
    public void testNotEmptyQueueSize() {
        QueueFromTwoStacks<Object> queue = createQueueFromTwoStacks(10);
        assertTrue("Size of queue with 10 items should be 10", queue.size() == 10);
    }

    @Test
    public void testInsertIntoEmptyQueue() {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        Object o = new Object();
        queue.insert(o);
        assertTrue("Size of queue with 1 item should be 1", queue.size() == 1);
        assertTrue("Inserted item should be the only item in the queue", queue.remove() == o);
    }

    @Test
    public void testInsertIntoNotEmptyQueue() {
        QueueFromTwoStacks<Object> queue = createQueueFromTwoStacks(10);
        int sizeBeforeInsert = queue.size();
        Object o = new Object();
        queue.insert(o);
        int sizeAfterInsert = queue.size();
        assertTrue("When inserting one item into NOT empty queue its size should increase by 1",
                (sizeAfterInsert - sizeBeforeInsert) == 1);
        assertTrue("Inserted item should not be the one to be returned first", queue.remove() != o);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveFromEmptyQueue() {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        Object o = queue.remove();
    }

    @Test
    public void testInsertRemove() {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        Object o1 = new Object();
        queue.insert(o1);
        Object o = queue.remove();
        assertTrue("When inserting one object it should be returned by remove method", o1 == o);
    }

    @Test
    public void testInsertInsertRemove() {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        Object o1 = new Object();
        Object o2 = new Object();
        queue.insert(o1);
        queue.insert(o2);
        Object o = queue.remove();
        assertTrue("After several insertions the first object should be returned by remove method", o1 == o);
    }

    @Test
    public void testInsertInsertRemoveInsertRemove() {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        Object o1 = new Object();
        Object o2 = new Object();
        Object o3 = new Object();
        queue.insert(o1);
        queue.insert(o2);
        queue.remove();
        queue.insert(o3);
        Object o = queue.remove();
        assertTrue("After several insertions and removals the correct object should be returned by remove method", o2 == o);
    }

    private static QueueFromTwoStacks<Object> createQueueFromTwoStacks(int size) {
        QueueFromTwoStacks<Object> queue = new QueueFromTwoStacks<>();
        for (int i = 0; i < size; i++) {
            queue.insert(new Object());
        }
        return queue;
    }

}
