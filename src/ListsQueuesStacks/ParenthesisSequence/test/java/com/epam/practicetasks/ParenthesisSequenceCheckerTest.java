package com.epam.practicetasks;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ParenthesisSequenceCheckerTest {
    private String s;
    private boolean result;

    private ParenthesisSequenceChecker psc;

    @Before
    public void initialize() {
        psc = new ParenthesisSequenceChecker();
    }

    public ParenthesisSequenceCheckerTest(String s, boolean result) {
        this.s = s;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {"", true},
                {"s1! ", true},
                {"()", true},
                {"()()", true},
                {"()[]", true},
                {"()[]{}", true},
                {"(())", true},
                {"[{}]", true},
                {"[{}]()", true},
                {"()[{}]", true},
                {"([]{})", true},
                {"([{}])", true},
                {"([(){}]{})", true},
                {"([(){()}]{[{}]()})", true},
                {"a(d[ss]d{dd}{[dsd]})[]a", true},
                {"(", false},
                {"((", false},
                {"))", false},
                {"(]", false},
                {"())", false},
                {"(()", false},
                {"([]", false},
                {"([)]", false},
                {"([{])", false},
                {"([{])", false},
                {"()[[]", false},
                {"()}[]", false},
                {"(})[]", false},
                {"{([{])}", false},
                {"{)([])}", false},
                {"a(d[ss]d{dd}{[ds]d]})[]a", false},
        });
    }

    @Test
    public void testParenthesisSequenceChacker() {
        Assert.assertEquals(result, psc.isCorrect(s));
    }
}
