package com.epam.practicetasks;

import java.util.HashMap;
import java.util.LinkedList;

public class ParenthesisSequenceChecker {

    public boolean isCorrect(String s) {
        LinkedList<Character> stack = new LinkedList();

        HashMap<Character, Character> map = new HashMap<>();
        map.put(')', '(');
        map.put(']', '[');
        map.put('}', '{');

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (map.containsValue(c)) {
                stack.push(c);
            }
            if (map.containsKey(c)) {
                if (stack.peek() == map.get(c)) stack.pop();
                else stack.push(c);
            }
        }

        return stack.isEmpty();
    }

    public static void main(String[] args) {
        ParenthesisSequenceChecker psc = new ParenthesisSequenceChecker();
        String s = "(d[ss]d{dd}{[dsd]})[]";
        System.out.println(s + "is correct: " + psc.isCorrect(s));

        s = "(d[ss]d{dd}{[ds]d]})[]";
        System.out.println(s + "is correct: " + psc.isCorrect(s));
    }

}
