package com.epam.practicetasks;

public class QueueFromArrayUser {

    public static void main(String[] args) {
        QueueFromArray<Integer> queue = new QueueFromArray(10);
        for (int i = 0; i < 10; i++) {
            queue.insert(i);
        }

        System.out.println("queue size = " + queue.size());
        System.out.println("queue = " + queue);

       for (int i = 0; i < 5; i++) {
            queue.remove();
        }

        for (int i = 10; i < 15; i++) {
            queue.insert(i);
        }

       System.out.println("queue size = " + queue.size());
       System.out.println("queue = " + queue);

    }
}
