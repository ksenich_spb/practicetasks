package com.epam.practicetasks;

import java.util.NoSuchElementException;

public class QueueFromArray<E> {

    Object[] items;

    private int maxSize;
    private int size;

    private int head = 0;
    private int tail = 0;

    public QueueFromArray(int maxSize) {
        if (maxSize > 0) {
            this.items = new Object[maxSize];
            this.maxSize = maxSize;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void insert(E e) {
        if (size == maxSize) throw new IllegalStateException();
        items[tail] = e;
        size++;
        if (tail == (maxSize - 1)) tail = 0;
        else tail++;
    }

    public E remove() {
        if (size == 0) throw new NoSuchElementException();
        E item = (E) items[head];
        items[head] = null;
        if (head == (maxSize - 1)) head = 0;
        else head++;
        size--;
        return item;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("[");
        int initialSize = size;
        for (int i = 0; i < initialSize; i++) {
            E item = remove();
            s.append(item + ";");
            insert(item);
        }
        s.append("]");
        return s.toString();
    }
}

