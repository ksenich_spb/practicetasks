package com.epam.practicetasks;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertTrue;

public class QueueFromArrayTest {

    @Test
    public void testEmptyQueueSize() {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        assertTrue("Size of empty queue should be zero", queue.size() == 0);
    }

    @Test
    public void testQueueWithFewItemsSize() {
        QueueFromArray<Object> queue = createQueueFromArray(5);
        assertTrue("Size of queue with 5 items should be 5", queue.size() == 5);
    }

    @Test
    public void testQueueSizeAfterManyOperations() {
        QueueFromArray<Object> queue = createQueueFromArray(10);
        for (int i = 0; i < 5; i++) {
            queue.remove();
            queue.insert(new Object());
        }
        assertTrue("Size of queue with 10 items should be 10", queue.size() == 10);
    }

    @Test
    public void testInsertIntoEmptyQueue() {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        Object o = new Object();
        queue.insert(o);
        assertTrue("Size of queue with 1 item should be 1", queue.size() == 1);
        assertTrue("Inserted item should be the only item in the queue", queue.remove() == o);
    }

    @Test
    public void testInsertIntoQueueWithFewItems() {
        QueueFromArray<Object> queue = createQueueFromArray(5);
        int sizeBeforeInsert = queue.size();
        Object o = new Object();
        queue.insert(o);
        int sizeAfterInsert = queue.size();
        assertTrue("When inserting one item into NOT empty queue its size should increase by 1",
                (sizeAfterInsert - sizeBeforeInsert) == 1);
        assertTrue("Inserted item should not be the one to be returned first", queue.remove() != o);
    }

    @Test
    public void testInsertIntoNotEmptyQueueWithManyItems() {
        QueueFromArray<Object> queue = createQueueFromArray(5);
        for (int i = 0; i < 7; i++) {
            queue.remove();
            queue.insert(new Object());
        }
        int sizeBeforeInsert = queue.size();
        Object o = new Object();
        queue.insert(o);
        int sizeAfterInsert = queue.size();
        assertTrue("When inserting one item into NOT empty queue its size should increase by 1",
                (sizeAfterInsert - sizeBeforeInsert) == 1);
        assertTrue("Inserted item should not be the one to be returned first", queue.remove() != o);
    }

    @Test(expected = IllegalStateException.class)
    public void testInsertIntoFullQueue() {
        QueueFromArray<Object> queue = createQueueFromArray(10);
        Object o = new Object();
        queue.insert(o);
    }

    @Test(expected = NoSuchElementException.class)
    public void testRemoveFromEmptyQueue() {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        Object o = queue.remove();
    }

    @Test
    public void testInsertRemove() {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        Object o1 = new Object();
        queue.insert(o1);
        Object o = queue.remove();
        assertTrue("When inserting one object it should be returned by remove method", o1 == o);
    }

    @Test
    public void testInsertInsertRemove() {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        Object o1 = new Object();
        Object o2 = new Object();
        queue.insert(o1);
        queue.insert(o2);
        Object o = queue.remove();
        assertTrue("After several insertions the first object should be returned by remove method", o1 == o);
    }

    @Test
    public void testInsertInsertRemoveInsertRemove() {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        Object o1 = new Object();
        Object o2 = new Object();
        Object o3 = new Object();
        queue.insert(o1);
        queue.insert(o2);
        queue.remove();
        queue.insert(o3);
        Object o = queue.remove();
        assertTrue("After several insertions and removals the correct object should be returned by remove method", o2 == o);
    }

    @Test
    public void testManyRemoves() {
        QueueFromArray<Object> queue = createQueueFromArray(10);
        for (int i = 0; i < 10; i++) {
            queue.remove();
        }
        Object o1 = new Object();
        queue.insert(o1);
        Object o = queue.remove();
        assertTrue("After several insertions and removals the correct object should be returned by remove method", o1 == o);
    }

    private static QueueFromArray<Object> createQueueFromArray(int size) {
        QueueFromArray<Object> queue = new QueueFromArray<>(10);
        for (int i = 0; i < size; i++) {
            queue.insert(new Object());
        }
        return queue;
    }
}
